#!/bin/bash
#------------------------------------------------------------------------------
#
# Install the compiled UBL 2.1 jar file in your local Maven repository.
#
#------------------------------------------------------------------------------
cd lib && mvn install:install-file -DgroupId=org.openinvoice.ubl -DartifactId=ubl -Dpackaging=jar -Dversion=2.1 -Dfile=ubl-2.1.jar && cd -

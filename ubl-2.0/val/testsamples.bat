@echo off
call validate ..\xsdrt\maindoc\UBL-CreditNote-2.0.xsd ..\xml\UBL-CreditNote-2.0-Example.xml
call validate ..\xsdrt\maindoc\UBL-DespatchAdvice-2.0.xsd ..\xml\UBL-DespatchAdvice-2.0-Example.xml
call validate ..\xsdrt\maindoc\UBL-Invoice-2.0.xsd ..\xml\UBL-Invoice-2.0-Example.xml
call validate ..\xsdrt\maindoc\UBL-Order-2.0.xsd ..\xml\UBL-Order-2.0-Example.xml
call validate ..\xsdrt\maindoc\UBL-OrderResponseSimple-2.0.xsd ..\xml\UBL-OrderResponseSimple-2.0-Example.xml
call validate ..\xsdrt\maindoc\UBL-Quotation-2.0.xsd ..\xml\UBL-Quotation-2.0-Example.xml
call validate ..\xsdrt\maindoc\UBL-ReceiptAdvice-2.0.xsd ..\xml\UBL-ReceiptAdvice-2.0-Example.xml
call validate ..\xsdrt\maindoc\UBL-RemittanceAdvice-2.0.xsd ..\xml\UBL-RemittanceAdvice-2.0-Example.xml
call validate ..\xsdrt\maindoc\UBL-RequestForQuotation-2.0.xsd ..\xml\UBL-RequestForQuotation-2.0-Example.xml
call validate ..\xsdrt\maindoc\UBL-Statement-2.0.xsd ..\xml\UBL-Statement-2.0-Example.xml
call validate ..\xsdrt\maindoc\UBL-ForwardingInstructions-2.0.xsd ..\xml\UBL-ForwardingInstructions-2.0-Example-International.xml
call validate ..\xsdrt\maindoc\UBL-Order-2.0.xsd ..\xml\UBL-Order-2.0-Example-International.xml
call validate ..\xsdrt\maindoc\UBL-Waybill-2.0.xsd ..\xml\UBL-Waybill-2.0-Example-International.xml

